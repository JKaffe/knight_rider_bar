## Ways I thought of implementing this:
1. Listing method:

   Create a list of binary numbers representing the KITT LED sequence and display each one, effectively represent the KITT LED sequence. The fading effect will be accounted for in the list with the benefit that we can easily achieve an n LED fading where n LEDs will be fading (flashing very fast).
   E.g.
   ```
      LEDs
   7654 3210
   ---------
   0001 0000
   0011 1000
   0001 0000
   0011 1000
   0001 0000
   ```
   This will flash LED 5 and 3, two times while LED 4 stays on for 5 display cycles effectively giving the flashing LEDs a duty cycle of 40 %.

   E.g. n bit fading using 2 bits for fading.
   ```
   0001 0000
   0011 1000
   0111 1100
   0011 1000
   0001 0000
   ```
   In generating the list we can easily change each LED's duty cycle to achieve the desired effect.

   Although this approach might result in a very fast executing code, the downside is the lack of learning and the challenge involved, furthermore the list of binary numbers will take a substantial amount of memory.

2. ORing shifts method:

   Starting with a number containing a single 1, such as 0000 1000b, shift left and right using RL/RLC, and  RR/RRC respectively, thus meeting requirement 1.
   In order to produce the fading effect we can OR the value together with the value shifted, producing the value which we can use to produce the fading effect.
   Which is why I have called this the "ORing shifts" method.
   To achieve the fading effect we can alternate between the original value and the generated and to achieve a finer grain control over the duty cycle we can use one of the internal timers.

   E.g. of the value generation.
   ```
   0001 0000
   0010 0000 // << 1
   0000 1000 // >> 1
   ---------
   0011 1000 // val OR (val << 1) OR (val >> 1)
   ```

   E.g of generating an n bit fading value.
   ```
   0001 0000
   0010 0000 // << 1
   0100 0000 // << 2
   0000 1000 // >> 1
   0000 0100 // >> 2
   ---------
   0111 1100 // val OR (val << 1) OR (val << 2) OR (val >> 1) OR (val >> 2)
   ```

   This approach might produce a slower executing code as it needs to first generate the values first. Furthermore it will likely produce complex code therefore hard to read and debug, however it might produce code with small memory requirements.
