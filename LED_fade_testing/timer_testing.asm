;    Knight Rider Bar - Knight 2000 style LED bar on an 8051.
;    Copyright (C) 2019 Karmjit Mahil
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <https://www.gnu.org/licenses/>.

ORG 8000h
	SJMP setup



	;Adjust to change refresh rate of the LEDs
preload_timer:
	MOV TH0, 06h
	MOV TL0, 6Eh	;Pre load timer
	RET

display:
	;Triggered on timer 1 overflow
	;Timer 0 ISR
	LCALL preload_timer

	;Display LED pattern
	MOV P1, R7

	;Resolve 'turning off' behaviour experienced  during testing
	NOP
	MOV P1, #00h

	RET



setup:
	SETB EA		;Enable Timer 0 interrupt
	SETB ET0


	;BUG HERE, mode 2 selected instead of mode 1
	;For more details look at LED_fade_testing.md

	MOV TMOD, #02h	;Select mode 1 for timer 0

	ACALL preload_timer

	SETB TR0	;Turn on timer 0. Enable displaying

	MOV R7, #0001000b

wait:	SJMP wait


ORG 0FF0Bh
	LCALL display
	RETI

END
