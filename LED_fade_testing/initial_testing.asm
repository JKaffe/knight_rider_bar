;    Knight Rider Bar - Knight 2000 style LED bar on an 8051.
;    Copyright (C) 2019 Karmjit Mahil
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <https://www.gnu.org/licenses/>.

ORG 8000h
	MOV P1, #00h	;Configuring P1 for output

	MOV DPTR, #ledSequence

restart:
	CLR A
	MOV R0, A

	MOV P1, #00h

displayLoop:
	MOV A, R0
	MOVC A, @A+DPTR
	MOV P1, A
	;ACALL delay
	INC R0
	CJNE R0, #06h, displayLoop

	SJMP restart

delay:
	MOV A, #07h
	MOV B, A

outerLoop:
	MOV A, #0FFh

contDelay:
	DJNZ ACC, contDelay
	DJNZ B, outerLoop
	RET

ledSequence:
	DB 00010000b, 00111000b, 00010000b, 00010000b, 00111000b, 01111100b, 00010000b

END
