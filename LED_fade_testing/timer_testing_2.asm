ORG 8000h
	SJMP setup



	;Adjust to change refresh rate of the LEDs
preload_timer:
	MOV TH0, #06h
	MOV TL0, #6Eh	;Pre load timer
	RET

display:
	;Triggered on timer 0 overflow
	;Timer 0 ISR
	;ACALL preload_timer

	;Display LED pattern
	MOV P1, R7

	;Resolve 'turning off' behaviour experienced  during testing
	NOP
	MOV P1, #00h

	INC R5
	CJNE R5, #04h, no_fade_effect

	;Fade effect.
	MOV R5, #00h

	MOV A, R6

	JB F0, right_going_sequence
	RR A
	SJMP save

right_going_sequence:
	RL A

save:
	XRL A, R7
	;MOV R7, A

	MOV P1, A
	NOP
	MOV P1, #00h
no_fade_effect:
	RET



setup:
	SETB EA		;Enable Timer 0 interrupt
	SETB ET0

	MOV TMOD, #02h	;Select mode 2 for timer 0
	MOV A, #0D0h
	MOV TH0, A
	MOV TL0, A

	;ACALL preload_timer

	SETB TR0	;Turn on timer 0. Enable displaying

	MOV A, #00010000b
	MOV R6, A
	MOV R7, A

	MOV R5, #00h

	CLR F0

wait:	SJMP wait


ORG 0FF0Bh
	LCALL display
	RETI

END
