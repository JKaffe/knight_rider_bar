# Knight Rider LED Bar.
KITT LED sequence reproduced on an 8051 microcontroller.

## Requirements:
1. Move the LED sequence left and right.
2. Produce a fading effect around the brightest LED.
3. Once one of the ends is reached, move the LED sequence the other way.
4. The fading should only be on one side, opposite the movement of the pattern.

## Licence
This project is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; version 3 of the License.
