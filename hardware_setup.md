## Hardware setup

The system is implemented on an [8051 micro-controller](https://en.wikipedia.org/wiki/Intel_MCS-51) using assembly.

The clock frequency is 11.0592 Mhz, and the machine cycle length is 1.085*10^-6 seconds.

The kit at my disposal already runs a monitor program which allows me to upload code into external RAM through the 8051's serial port, and execute it.
Because of this monitor code the vector table is as follows:

| Name                  | Address |
| --------------------- | ------- |
| Reset                 | 0xff00  |
| External Interrupt 0  | 0xff03  |
| Timer 0 Interrupt     | 0xff0b  |
| External Interrupt 1  | 0xff13  |
| Timer 1 Interrupt     | 0xff1b  |
| Serial Port Interrupt | 0xff23  |

The ORG (origin) directive for my code will always be `ORG 8000h` so that it does not interfere with the monitor code.

The LEDs which will show the KITT bar sequence are connected to port 1.

The only available inputs are four active high buttons connected to port 3 pin 2 to pin 5.

| Bit  | Name                                               |
| ---- | -------------------------------------------------- |
| P3.2 | <span style="text-decoration:overline">INT0</span> |
| P3.3 | <span style="text-decoration:overline">INT1</span> |
| P3.4 | T0                                                 |
| P3.5 | T1                                                 |

