# Fade testing Notes.
## Description.
   These notes will contain details and observations made during a trial testing on how to achieve the fading effect.

## Initial testing.
   - P1 has to be cleared (set to 0x00) for a certain amount of time during the sequence as otherwise LEDs that are supposed to stay on for the whole sequence, turn off indefinitely after a random amount of time.

   - This turning off behaviour was only noticed with P1.4, when P1.4 was the centre of the sequence. However this could not be observed with P1.5 being in the centre.

   - It can be beneficial to clear P1 regularly although the period for which it has to be cleared is not yet known.

   - The code as of now, does not produce a sufficiently satisfying fading effect. The difference in brightness is not yet satisfactory.

   - P1 has to be cleared for a specific, yet not know period of time in order to assure correct working of the LEDs.

   - Timers will be used to refresh the LEDs with the next binary value, thus simultaneously resolving the "turn off" behaviour experienced in the.

## With timers.
   - The LED's brightness has substantially decreased.

   - Because of the fast refresh rate, the next value to show cannot be calculated each time, as otherwise each LED would flash very quickly (because of the rotating action).

   - A separate timer can be used, with higher priority interrupt, to generate the new sequence value.

   - Value generation frequency calculations:

     ```
     f(T) = T / (1.085 * 10^-6)

     T = 100 ms, f = 10Hz
     f(0.1) = 92165.89... -> 92166 -> 1 0110 1000 0000 0110 -> 0x16806
     Not feasable because of 16 bit limit.

     T = 50 ms, f = 20Hz
     f(0.05) = 46082.94... -> 46083 -> 1011 0100 0000 0011 -> 0xB403
     Feasable with mode 1 only.

     T = 25ms, f = 40Hz
     f(0.025) = 23041.47... -> 23041 -> 0101 1010 0000 0001 -> 0x5A01
     Feasable with mode 1 only.
     ```

   - A register can be used as a counter so that at each timer overflow, is incremented and depending on its value the next number is generated, effectively achieving a granularity of X (chosen period) ms, and `kX where k -> R, k > 0 ` ms between the generation of the next value.

## Timer testing 2

   - There is a bug in the code for the timer testing. The timer is set to mode 2 instead of mode 1 therefore the refresh rate was different than what I thought.

   - Fixing the bug set the refresh rate to 560Hz however the LED is very dim, therefore a higher refresh rate will be required.

   - After some further testing I determined that having the timer in mode 2 with 0xD0 pre loaded achieves a satisfying brightness.

   - To achieve the fading effect I came up with a different approach of XORing values. This will be documented later on.

   - Dimming of an LED to achieve the fading effect was successful however the LED was not dim enough.

   - Switching on and off the fading LED every X amount of timer overflows, so when the LED is turned on it is turned off after X amount of timer overflows, did not provide enough dimming. The fading was not satisfactory.

   - Flashing (turning on and directly off) the LED every X amount of timer overflows provided more dimming, therefore a better fading effect.

   - The lighting of the room has a big impact on the fading effect. This problem could be solved by enclosing the LED module.

   - With some trial and error I determined that a scaler of 0x04 gave the best fading effect in the lighting environment I am in.

## LED refresh/flickering rate.
During timer testing 2 it was determined that a preload of 0xD0 and a scaler of 0x04 gave a satisfying output.
```
T = (0xFF + 1 - 0xD0) * (1.085 * 10^-6) = 48 * 1.085 * 10^-6 = 0.00005208 -> 52.08us

4 * 52.08 = 208.32 us
```
The brightest LED flashes every 52.08us and the fading LED flashes every 208.32us.

## Conclusions
Satisfactory fading was achieved, therefore fade testing is complete.
